package com.qingxun.core.interceptor;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.WebUtils;

import com.qingxun.core.util.UserUtil;

public class SessionTimeoutInterceptor implements HandlerInterceptor   {
	private static Log log=LogFactory.getLog(SessionTimeoutInterceptor.class);
	private List<String> allowUrls = new ArrayList<String>();
	public List<String> getAllowUrls() {
		return allowUrls;
	}
	public void setAllowUrls(List<String> allowUrls) {
		this.allowUrls = allowUrls;
	}

	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, 
			Object handler) throws Exception {
		String requestUrl = request.getRequestURI();
		for(String url : allowUrls) {
			if(requestUrl.endsWith(url)) {
				return true;
			}
		}
//		log.info("当前用户"+UserUtil.getCurrentUser());
//		log.info("session超时=-================"+UserUtil.getHttpSession().getTimeout());
		if(UserUtil.getCurrentUser() != null) {
			return true;
		}else {
			throw new SessionTimeoutException("Session超时，请重新登录!");
		}
	}
	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, 
			Object handler, ModelAndView modelAndView) throws Exception {
		
	}
	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, 
			Object handler, Exception ex) throws Exception {
		
	}
}