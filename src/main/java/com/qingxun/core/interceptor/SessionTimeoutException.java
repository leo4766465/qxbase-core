package com.qingxun.core.interceptor;

import java.io.IOException;

public class SessionTimeoutException extends Exception {
    /** 
     *  
     */  
    private static final long serialVersionUID = 1L;  
  
    public SessionTimeoutException() {  
        super();  
        // TODO Auto-generated constructor stub  
    }  
  
    public SessionTimeoutException(String message) throws IOException {  
        super(message);  
        // TODO Auto-generated constructor stub  
    }
}
