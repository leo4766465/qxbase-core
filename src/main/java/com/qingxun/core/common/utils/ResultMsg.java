package com.qingxun.core.common.utils;

import java.io.Serializable;

/**
 * Service 返回统一的消息格式
 * 
 * @author Allen
 *
 * @param <T>
 */
public class ResultMsg<T> implements Serializable {

	private static final long serialVersionUID = 929000570441785903L;
	
	public static final String MSG_SUCCESS_DFT = "SUCCESSFUL";

	private int code; // 错误码
	private String msg;// 错误信息
	private T data;// 数据
//	private Map<String, Object> props = new HashMap<String, Object>();
	public ResultMsg() {
	}

	public ResultMsg(int code) {
		this.code = code;
	}

	public ResultMsg(int code, String msg) {
		this.code = code;
		this.msg = msg;
	}

	public ResultMsg(int code, String msg, T data) {
		this.code = code;
		this.msg = msg;
		this.data = data;
	}

	public ResultMsg(T data) {
		this.code = RespCode.SUCCESS.getCode();
		this.msg = MSG_SUCCESS_DFT;
		this.data = data;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

//	public Map<String, Object> getProps() {
//		return props;
//	}
//
//	public void setProps(Map<String, Object> props) {
//		this.props = props;
//	}

	public enum RespCode {
		SUCCESS(1), EXCEPTION(-1), FAILED(0),UNAUTHORIZED(401);
		public int value;
    
		RespCode(int value) {
			this.value = value;
		}

		public int getCode() {
			return value;
		}
	}

}
