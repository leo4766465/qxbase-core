package com.qingxun.core.task;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.qingxun.core.system.entity.ScheduleJob;
/**
 * 清理日志
 * @author leejing.sh
 * @date 2015年1月13日
 */
public class ScanProjectJob extends AbstractJob{
	public void execute(JobExecutionContext jobexecutioncontext) throws JobExecutionException {
		// TODO Auto-generated method stub
        ScheduleJob scheduleJob = (ScheduleJob)jobexecutioncontext.getMergedJobDataMap().get("scheduleJob");
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy年MM月dd日 HH时mm分ss秒");    
	    System.out.println("任务名称 = [" + scheduleJob.getName() + "]"+ " 在 " + dateFormat.format(new Date())+" 时运行");

//	    ProjectService projectService=(ProjectService)this
//	    		.getApplicationContext(jobexecutioncontext)
//	    		.getBean("projectService");
//	    projectService.upProjectStatus();
	}
}
