package com.qingxun.core.util;

import java.util.Properties;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;

/**
 * 发邮件工具类
 * @author leo
 * 
 */
public class JavaMailUtil {
	public static final String host="smtp.163.com";
	public static final String account="17612108076";
	public static final String pwd="qingxun0083";
	public static final String from="m17612108076@163.com";
	private static JavaMailUtil javaMailUtil;
	//固定参数
	private MimeMessageHelper messageHelper;
	private MimeMessage mailMessage;
	private JavaMailSenderImpl senderImpl;
	//单例模式
	public static JavaMailUtil getInstanceof(){
		if(javaMailUtil==null){
			javaMailUtil=new JavaMailUtil();
			javaMailUtil.init();
		}
		return javaMailUtil;
	}
	
	//初始化
	private void init(){
		senderImpl = new JavaMailSenderImpl();
		// 设定mail server
		senderImpl.setHost(host);
		senderImpl.setUsername(account); // 根据自己的情况,设置username
		senderImpl.setPassword(pwd); // 根据自己的情况, 设置password
		senderImpl.setDefaultEncoding("UTF-8");
		// 建立邮件消息,发送简单邮件和html邮件的区别
		mailMessage = senderImpl.createMimeMessage();
		messageHelper = new MimeMessageHelper(mailMessage);

		Properties prop = new Properties();
		prop.put("mail.smtp.auth", "true"); // 将这个参数设为true，让服务器进行认证,认证用户名和密码是否正确
		prop.put("mail.smtp.timeout", "25000");
		senderImpl.setJavaMailProperties(prop);
	}
	
	public String sendMail(String subject,String to,String content){
		try {
			// 设置收件人，寄件人
			messageHelper.setTo(to);
			messageHelper.setFrom(from);
			messageHelper.setSubject(subject);
			// true 表示启动HTML格式的邮件
			messageHelper.setText(content,true);
			// 发送邮件
			senderImpl.send(mailMessage);
			System.out.println("邮件发送成功..");
			return "success";
		} catch (MailException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "fail";
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "fail";
		}
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception {
		JavaMailUtil jm=JavaMailUtil.getInstanceof();
		StringBuffer strbuffer=new StringBuffer();
		strbuffer.append("<!DOCTYPE html>");
		strbuffer.append("<html>");
		strbuffer.append("<head>");
		strbuffer.append("<meta charset=\"UTF-8\">");
		strbuffer.append("<title>Basic CRUD Application - jQuery EasyUI CRUD Demo</title>");
		strbuffer.append("<link rel=\"stylesheet\" type=\"text/css\" href=\"http://www.jeasyui.com/easyui/themes/default/easyui.css\">");
		strbuffer.append("<link rel=\"stylesheet\" type=\"text/css\" href=\"http://www.jeasyui.com/easyui/themes/icon.css\">");
		strbuffer.append("<link rel=\"stylesheet\" type=\"text/css\" href=\"http://www.jeasyui.com/easyui/themes/color.css\">");
		strbuffer.append("<link rel=\"stylesheet\" type=\"text/css\" href=\"http://www.jeasyui.com/easyui/demo/demo.css\">");
		strbuffer.append("<script type=\"text/javascript\" src=\"http://code.jquery.com/jquery-1.6.min.js\"></script>");
		strbuffer.append("<script type=\"text/javascript\" src=\"http://www.jeasyui.com/easyui/jquery.easyui.min.js\"></script>");
//		strbuffer.append("<style>");
//		strbuffer.append("*{");
//		strbuffer.append("	font-size:12px;");
//		strbuffer.append("}");
//		strbuffer.append("body {");
//		strbuffer.append("    font-family:verdana,helvetica,arial,sans-serif;");
//		strbuffer.append("    padding:20px;");
//		strbuffer.append("    font-size:12px;");
//		strbuffer.append("    margin:0;");
//		strbuffer.append("}");
//		strbuffer.append("h2 {");
//		strbuffer.append("    font-size:18px;");
//		strbuffer.append("    font-weight:bold;");
//		strbuffer.append("   margin:0;");
//		strbuffer.append("   margin-bottom:15px;");
//		strbuffer.append("}");
//		strbuffer.append(".demo-info{");
//		strbuffer.append("	padding:0 0 12px 0;");
//		strbuffer.append("}");
//		strbuffer.append(".demo-tip{");
//		strbuffer.append("display:none;");
//		strbuffer.append("}");
//		strbuffer.append("</style>");
//		
		strbuffer.append("</head>");
		strbuffer.append("<body>");
		strbuffer.append("<h2>Basic CRUD Application</h2>");
		strbuffer.append("<p>Click the buttons on datagrid toolbar to do crud actions.</p>");	
		strbuffer.append("<table id=\"dg\" title=\"My Users\" class=\"easyui-datagrid\" style=\"width:700px;height:250px\"");
		strbuffer.append("url=\"get_users.php\"");
		strbuffer.append("           toolbar=\"#toolbar\" pagination=\"true\"");
		strbuffer.append("rownumbers=\"true\" fitColumns=\"true\" singleSelect=\"true\">");
		strbuffer.append("<thead>");
		strbuffer.append("<tr>");
		strbuffer.append("<th field=\"firstname\" width=\"50\">First Name</th>");
		strbuffer.append("<th field=\"lastname\" width=\"50\">Last Name</th>");
		strbuffer.append("<th field=\"phone\" width=\"50\">Phone</th>");
		strbuffer.append("<th field=\"email\" width=\"50\">Email</th>");
		strbuffer.append("</tr>");
		strbuffer.append("</thead>");
		strbuffer.append("</table>");
		strbuffer.append(" <div id=\"toolbar\">");
		strbuffer.append(
				"       <a href=\"javascript:void(0)\" class=\"easyui-linkbutton\" iconCls=\"icon-add\" plain=\"true\" onclick=\"newUser()\">New User</a>");
		strbuffer.append(
				"      <a href=\"javascript:void(0)\" class=\"easyui-linkbutton\" iconCls=\"icon-edit\" plain=\"true\" onclick=\"editUser()\">Edit User</a>");
		strbuffer.append(
				"       <a href=\"javascript:void(0)\" class=\"easyui-linkbutton\" iconCls=\"icon-remove\" plain=\"true\" onclick=\"destroyUser()\">Remove User</a>");
		strbuffer.append("  </div>");

		strbuffer.append(
				"   <div id=\"dlg\" class=\"easyui-dialog\" style=\"width:400px;height:280px;padding:10px 20px\"");
		strbuffer.append("          closed=\"true\" buttons=\"#dlg-buttons\">");
		strbuffer.append("      <div class=\"ftitle\">User Information</div>");
		strbuffer.append("       <form id=\"fm\" method=\"post\" novalidate>");
		strbuffer.append("           <div class=\"fitem\">");
		strbuffer.append("             <label>First Name:</label>");
		strbuffer.append(" <input name=\"firstname\" class=\"easyui-textbox\" required=\"true\">");
		strbuffer.append(" </div>");
		strbuffer.append("<div class=\"fitem\">");
		strbuffer.append("<label>Last Name:</label>");
		strbuffer.append(" <input name=\"lastname\" class=\"easyui-textbox\" required=\"true\">");
		strbuffer.append("</div>");
		strbuffer.append("<div class=\"fitem\">");
		strbuffer.append("<label>Phone:</label>");
		strbuffer.append("<input name=\"phone\" class=\"easyui-textbox\">");
		strbuffer.append(" </div>");
		strbuffer.append("<div class=\"fitem\">");
		strbuffer.append("<label>Email:</label>");
		strbuffer.append(" <input name=\"email\" class=\"easyui-textbox\" validType=\"email\">");
		strbuffer.append(" </div>");
		strbuffer.append(" </form>");
		strbuffer.append("</div>");
//		strbuffer.append("<div id=\"dlg-buttons\">");
//		strbuffer.append(
//				"    <a href=\"javascript:void(0)\" class=\"easyui-linkbutton c6\" iconCls=\"icon-ok\" onclick=\"saveUser()\" style=\"width:90px\">Save</a>");
//		strbuffer.append(
//				"    <a href=\"javascript:void(0)\" class=\"easyui-linkbutton\" iconCls=\"icon-cancel\" onclick=\"javascript:$('#dlg').dialog('close')\" style=\"width:90px\">Cancel</a>");
//		strbuffer.append("</div>");
//		strbuffer.append("<script type=\"text/javascript\">");
//		strbuffer.append(" var url;");
//		strbuffer.append("function newUser(){");
//		strbuffer.append("     $('#dlg').dialog('open').dialog('center').dialog('setTitle','New User');");
//		strbuffer.append("   $('#fm').form('clear');");
//		strbuffer.append("   url = 'save_user.php';");
//		strbuffer.append(" }");
//		strbuffer.append(" function editUser(){");
//		strbuffer.append(" var row = $('#dg').datagrid('getSelected');");
//		strbuffer.append(" if (row){");
//		strbuffer.append("  $('#dlg').dialog('open').dialog('center').dialog('setTitle','Edit User');");
//		strbuffer.append("   $('#fm').form('load',row);");
//		strbuffer.append("  url = 'update_user.php?id='+row.id;");
//		strbuffer.append(" }");
//		strbuffer.append("  }");
//		strbuffer.append("  function saveUser(){");
//		strbuffer.append("    $('#fm').form('submit',{");
//		strbuffer.append("    url: url,");
//		strbuffer.append("    onSubmit: function(){");
//		strbuffer.append("        return $(this).form('validate');");
//		strbuffer.append("    },");
//		strbuffer.append("   success: function(result){");
//		strbuffer.append("      var result = eval('('+result+')');");
//		strbuffer.append("   if (result.errorMsg){");
//		strbuffer.append("      $.messager.show({");
//		strbuffer.append("         title: 'Error',");
//		strbuffer.append("          msg: result.errorMsg");
//		strbuffer.append("      });");
//		strbuffer.append("  } else {");
//		strbuffer.append("      $('#dlg').dialog('close');        // close the dialog");
//		strbuffer.append("      $('#dg').datagrid('reload');    // reload the user data");
//		strbuffer.append("  }");
//		strbuffer.append("  }");
//		strbuffer.append(" });");
//		strbuffer.append("  }");
//		strbuffer.append(" function destroyUser(){");
//		strbuffer.append("  var row = $('#dg').datagrid('getSelected');");
//		strbuffer.append(" if (row){");
//		strbuffer.append("  $.messager.confirm('Confirm','Are you sure you want to destroy this user?',function(r){");
//		strbuffer.append("     if (r){");
//		strbuffer.append("        $.post('destroy_user.php',{id:row.id},function(result){");
//		strbuffer.append("           if (result.success){");
//		strbuffer.append("             $('#dg').datagrid('reload');    // reload the user data");
//		strbuffer.append("         } else {");
//		strbuffer.append("         $.messager.show({    // show error message");
//		strbuffer.append("             title: 'Error',");
//		strbuffer.append("             msg: result.errorMsg");
//		strbuffer.append("         });");
//		strbuffer.append("      }");
//		strbuffer.append("   },'json');");
//		strbuffer.append(" }");
//		strbuffer.append("  });");
//		strbuffer.append("   }");
//		strbuffer.append("  }");
//		strbuffer.append("  </script>");
//		strbuffer.append("    <style type=\"text/css\">");
//		strbuffer.append("      #fm{");
//		strbuffer.append("          margin:0;");
//		strbuffer.append("         padding:10px 30px;");
//		strbuffer.append("   }");
//		strbuffer.append("   .ftitle{");
//		strbuffer.append("       font-size:14px;");
//		strbuffer.append("       font-weight:bold;");
//		strbuffer.append("       padding:5px 0;");
//		strbuffer.append("       margin-bottom:10px;");
//		strbuffer.append("       border-bottom:1px solid #ccc;");
//		strbuffer.append("   }");
//		strbuffer.append("   .fitem{");
//		strbuffer.append("      margin-bottom:5px;");
//		strbuffer.append("  }");
//		strbuffer.append("  .fitem label{");
//		strbuffer.append("    display:inline-block;");
//		strbuffer.append("      width:80px;");
//		strbuffer.append(" }");
//		strbuffer.append(" .fitem input{");
//		strbuffer.append("    width:160px;");
//		strbuffer.append(" }");
//		strbuffer.append("  </style>");
		strbuffer.append("</body>");
		strbuffer.append("</html>");
		
		System.out.println(strbuffer.toString());
		jm.sendMail("项目日报", "921640763@qq.com",strbuffer.toString());
	}
}