package com.qingxun.core.util;

import java.util.HashMap;
import java.util.Random;
import java.util.Set;

import com.cloopen.rest.sdk.CCPRestSDK;

public class SMSUtil {
	public static String ACCOUNT_SID="";
	public static String AUTH_TOKEN="";
	public static String APP_ID="";
	private static SMSUtil smsUtil=null;
	public static SMSUtil getInstanceof(){
		if(smsUtil==null){
			smsUtil=new SMSUtil();
			smsUtil.init();
		}
		return smsUtil;
	}
	
	CCPRestSDK restAPI =null;
	private void init(){
	    restAPI = new CCPRestSDK();
		restAPI.init("sandboxapp.cloopen.com", "8883");// 初始化服务器地址和端口，格式如下，服务器地址不需要写https://
		restAPI.setAccount(ACCOUNT_SID, AUTH_TOKEN);// 初始化主帐号和主帐号TOKEN
		restAPI.setAppId(APP_ID);// 初始化应用ID
	}
	
	public static void main(String[] args){
//		SMSUtil.getInstanceof().sendSMS("13671703746", "1223", 1);
	}
	
	public void sendSMS(String mobile,String templateId,String code,int time){
		HashMap<String, Object> result = null;
		result = restAPI.sendTemplateSMS(mobile,templateId ,new String[]{code,""+time});
		System.out.println("SDKTestSendTemplateSMS result=" + result);
		if("000000".equals(result.get("statusCode"))){
			//正常返回输出data包体信息（map）
			HashMap<String,Object> data = (HashMap<String, Object>) result.get("data");
			Set<String> keySet = data.keySet();
			for(String key:keySet){
				Object object = data.get(key);
				System.out.println(key +" = "+object);
			}
		}else{
			//异常返回输出错误码和错误信息
			System.out.println("错误码=" + result.get("statusCode") +" 错误信息= "+result.get("statusMsg"));
		}
	}
	
	public static String genCode(int len){
        int max=9;
        int min=0;
        Random random = new Random();
        String str="";
        for(int i=0;i<len;i++){
           str+= random.nextInt(max)%(max-min+1) + min;      	
        }
        return str;
	}		
}
