package com.qingxun.core.system.dao;

import org.springframework.stereotype.Repository;

import com.qingxun.core.common.persistence.HibernateDao;
import com.qingxun.core.system.entity.AreaInfo;

/**
 * 区域DAO
 * @author ty
 * @date 2015年1月13日
 */
@Repository
public class AreaInfoDao extends HibernateDao<AreaInfo, Integer>{

}
