package com.qingxun.core.system.dao;

import org.springframework.stereotype.Repository;

import com.qingxun.core.common.persistence.HibernateDao;
import com.qingxun.core.system.entity.Dict;

/**
 * 字典DAO
 * @author ty
 * @date 2015年1月13日
 */
@Repository
public class DictDao extends HibernateDao<Dict, Integer>{

}
