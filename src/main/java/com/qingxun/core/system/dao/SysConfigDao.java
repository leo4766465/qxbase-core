package com.qingxun.core.system.dao;

import org.springframework.stereotype.Repository;

import com.qingxun.core.common.persistence.HibernateDao;
import com.qingxun.core.system.entity.SysConfig;

@Repository
public class SysConfigDao extends HibernateDao<SysConfig, Long>{

}
