package com.qingxun.core.system.dao;

import org.springframework.stereotype.Repository;

import com.qingxun.core.common.persistence.HibernateDao;
import com.qingxun.core.system.entity.Role;


/**
 * 角色DAO
 * @author ty
 * @date 2015年1月13日
 */
@Repository
public class RoleDao extends HibernateDao<Role, Integer>{

}
