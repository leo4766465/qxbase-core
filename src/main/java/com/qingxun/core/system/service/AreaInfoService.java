package com.qingxun.core.system.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.qingxun.core.common.persistence.HibernateDao;
import com.qingxun.core.common.service.BaseService;
import com.qingxun.core.system.dao.AreaInfoDao;
import com.qingxun.core.system.entity.AreaInfo;
import com.qingxun.core.system.entity.Dict;

/**
 * 区域service
 * @author ty
 * @date 2015年1月13日
 */
@Service
@Transactional(readOnly=true)
public class AreaInfoService extends BaseService<AreaInfo, Integer> {
	
	@Autowired
	private AreaInfoDao areaInfoDao;

	@Override
	public HibernateDao<AreaInfo, Integer> getEntityDao() {
		return areaInfoDao;
	}
}
