package com.qingxun.core.system.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.qingxun.core.common.persistence.HibernateDao;
import com.qingxun.core.common.service.BaseService;
import com.qingxun.core.system.dao.SysConfigDao;
import com.qingxun.core.system.entity.SysConfig;

@Service
@Transactional(readOnly=true)
public class SysConfigService extends BaseService<SysConfig, Long>{
	@Autowired
	private SysConfigDao sysConfigDao;
	
	@Override
	public HibernateDao<SysConfig, Long> getEntityDao() {
		// TODO Auto-generated method stub
		return sysConfigDao;
	}

}
