package com.qingxun.core.system.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.qingxun.core.common.persistence.HibernateDao;
import com.qingxun.core.common.service.BaseService;
import com.qingxun.core.system.dao.LogDao;
import com.qingxun.core.system.entity.Log;

/**
 * 日志service
 * @author ty
 * @date 2015年1月14日
 */
@Service
@Transactional(readOnly=true)
public class LogService extends BaseService<Log, Integer> {
	
	@Autowired
	private LogDao logDao;
	
	@Override
	public HibernateDao<Log, Integer> getEntityDao() {
		return logDao;
	}

//	public Map<String, Object> search(Page<Log> page,String text) {
//        FullTextSession fullTextSession = Search.getFullTextSession(this.logDao.getSession());
////        List<Log> result=new ArrayList<Log>();
//        //返回数据
//		Map<String, Object> map = new HashMap<String, Object>();
//        try {
//			fullTextSession.createIndexer().startAndWait();
//	        SearchFactory sf = fullTextSession.getSearchFactory();
//	        QueryBuilder qb = sf.buildQueryBuilder().forEntity(Log.class).get();
//	        org.apache.lucene.search.Query luceneQuery  = qb.keyword()
//	        		.onFields("operationCode","requestParam","description")
//	        		.matching(text).createQuery();
//	        //查询
//	        FullTextQuery hibQuery = fullTextSession.createFullTextQuery(luceneQuery);
//	        hibQuery.setFirstResult((page.getPageNo()-1)* page.getPageSize());
//	        hibQuery.setMaxResults(page.getPageSize());
//	        //分装返回数据
//			map.put("rows", hibQuery.list());
//			map.put("total", hibQuery.getResultSize());
//			logger.info("hibQuery--------------:"+hibQuery.getResultSize());
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//        return map;
//	}
	
	/**
	 * 批量删除日志
	 * @param idList
	 */
	@Transactional(readOnly=false)
	public void deleteLog(List<Integer> idList){
		logDao.deleteBatch(idList);
	}
	
}
