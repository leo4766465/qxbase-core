package com.qingxun.core.system.web;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.qingxun.core.common.persistence.Page;
import com.qingxun.core.common.persistence.PropertyFilter;
import com.qingxun.core.system.entity.AreaInfo;
import com.qingxun.core.system.service.AreaInfoService;

/**
 * 区域controller
 * @author ty
 * @date 2015年1月13日
 */
@Controller
@RequestMapping("system/area")
public class AreaInfoController extends BaseController{
	
	@Autowired
	private AreaInfoService areaInfoService;
	
	/**
	 * 默认页面
	 */
	@RequestMapping(method = RequestMethod.GET)
	public String list() {
		return "system/areaInfoList";
	}

	/**
	 * 获取区域json
	 */
	@RequiresPermissions("sys:areainfo:view")
	@RequestMapping(value="json",method = RequestMethod.GET)
	@ResponseBody
	public List<AreaInfo> areaInfoList(HttpServletRequest request) {
//		Page<AreaInfo> page = getPage(request);
//		List<PropertyFilter> filters = PropertyFilter.buildFromHttpRequest(request);
//		page = areaInfoService.search(page, filters);
//		return getEasyUIData(page);
		return areaInfoService.getEntityDao().find("from AreaInfo order by sort");
	}
	
	/**
	 * 添加区域跳转
	 * 
	 * @param model
	 */
	@RequiresPermissions("sys:areainfo:add")
	@RequestMapping(value = "create", method = RequestMethod.GET)
	public String createForm(Model model) {
		model.addAttribute("areaInfo", new AreaInfo());
		model.addAttribute("action", "create");
		return "system/areaInfoForm";
	}

	/**
	 * 添加区域
	 * 
	 * @param areaInfo
	 * @param model
	 */
	@RequiresPermissions("sys:areainfo:add")
	@RequestMapping(value = "create", method = RequestMethod.POST)
	@ResponseBody
	public String create(@Valid AreaInfo areaInfo, Model model) {
		areaInfoService.save(areaInfo);
		return "success";
	}

	/**
	 * 修改区域跳转
	 * 
	 * @param id
	 * @param model
	 * @return
	 */
	@RequiresPermissions("sys:areainfo:update")
	@RequestMapping(value = "update/{id}", method = RequestMethod.GET)
	public String updateForm(@PathVariable("id") Integer id, Model model) {
		model.addAttribute("areaInfo", areaInfoService.get(id));
		model.addAttribute("action", "update");
		return "system/areaInfoForm";
	}

	/**
	 * 修改区域
	 * 
	 * @param areaInfo
	 * @param model
	 * @return
	 */
	@RequiresPermissions("sys:areainfo:update")
	@RequestMapping(value = "update", method = RequestMethod.POST)
	@ResponseBody
	public String update(@Valid @ModelAttribute @RequestBody AreaInfo areaInfo,Model model) {
		areaInfoService.update(areaInfo);
		return "success";
	}

	/**
	 * 删除区域
	 * 
	 * @param id
	 * @return
	 */
	@RequiresPermissions("sys:areainfo:delete")
	@RequestMapping(value = "delete/{id}")
	@ResponseBody
	public String delete(@PathVariable("id") Integer id) {
		areaInfoService.delete(id);
		return "success";
	}
	
	@ModelAttribute
	public void getAreaInfo(@RequestParam(value = "id", defaultValue = "-1") Integer id,Model model) {
		if (id != -1) {
			model.addAttribute("areaInfo", areaInfoService.get(id));
		}
	}

}
