package com.qingxun.core.system.web;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.qingxun.core.common.persistence.Page;
import com.qingxun.core.common.persistence.PropertyFilter;
import com.qingxun.core.excel.ExcelUtils;
import com.qingxun.core.excel.JsGridReportBase;
import com.qingxun.core.excel.TableData;
import com.qingxun.core.system.entity.Log;
import com.qingxun.core.system.service.LogService;
import com.qingxun.core.util.UserUtil;

/**
 * 日志controller
 * @author ty
 * @date 2015年1月14日
 */
@Controller
@RequestMapping("system/log")
public class LogController extends BaseController{

	@Autowired
	private LogService logService;
	
	/**
	 * 默认页面
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET)
	public String list(){
		return "system/logList";
	}
	
	/**
	 * 默认页面
	 * @return
	 */
	@RequestMapping(value="searchList",method = RequestMethod.GET)
	public String search(){
		return "system/logSearchList";
	}	
	
	/**
	 * 获取日志json
	 */
	@RequiresPermissions("sys:log:view")
	@RequestMapping("json")
	@ResponseBody
	public Map<String, Object> json(HttpServletRequest request) {
		Page<Log> page = getPage(request);
		List<PropertyFilter> filters = PropertyFilter.buildFromHttpRequest(request);
		page = logService.search(page, filters);
		return getEasyUIData(page);
	}
	
	/**
	 * 获取日志json
	 */
//	@RequiresPermissions("sys:log:view")
//	@RequestMapping("jsonForSearch")
//	@ResponseBody
//	public Map<String, Object> jsonForSearch(HttpServletRequest request,String k) {
//		Page<Log> page = getPage(request);
//		log.info("搜索------------:"+k);
//        if(null!=k&&!k.equals("")){
//        	log.info("不为空-----------------");
//        	return logService.search(page, k);
//        }
//    	page=logService.getEntityDao().findPage(page);
//    	return getEasyUIData(page);
//	}	
	
	/**
	 * 删除日志
	 * @param id
	 */
	@RequiresPermissions("sys:log:delete")
	@RequestMapping(value = "delete/{id}")
	@ResponseBody
	public String delete(@PathVariable("id") Integer id) {
		logService.delete(id);
		return "success";
	}
	
	/**
	 * 批量删除日志
	 * @param idList
	 */
	@RequestMapping(value = "delete", method = RequestMethod.POST)
	@ResponseBody
	public String delete(@RequestBody List<Integer> idList) {
		logService.deleteLog(idList);
		return "success";
	}

	/**
	 * 导出excel
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("exportExcel")
	public void exportExcel(HttpServletRequest request, HttpServletResponse response) throws Exception{
		response.setContentType("application/msexcel;charset=GBK");
        
        List<Log> list = logService.getAll();//获取数据
        
        String title = "log";
        String[] hearders = new String[] {"操作编码", "详细描述", "执行时间(mm)", "操作系统", "浏览器", "IP","MAC","操作者","操作时间"};//表头数组
        String[] fields = new String[] {"operationCode", "description", "executeTime", "os", "browser", "ip","mac","creater","createDate"};//People对象属性数组
        TableData td = ExcelUtils.createTableData(list, ExcelUtils.createTableHeader(hearders),fields);
        JsGridReportBase report = new JsGridReportBase(request, response);
        report.exportToExcel(title, UserUtil.getCurrentUser().getName(), td);
	}
}
