package com.qingxun.core.system.web;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.qingxun.core.common.persistence.Page;
import com.qingxun.core.common.persistence.PropertyFilter;
import com.qingxun.core.system.entity.SysConfig;
import com.qingxun.core.system.service.SysConfigService;

/**
 * 系统设置
 * @author ty
 * @date 2015年1月13日
 */
@Controller
@RequestMapping("sysConfig")
public class SysConfigController extends BaseController {

	@Autowired
	private SysConfigService sysConfigService;

	/**
	 * 默认页面
	 */
	@RequestMapping(method = RequestMethod.GET)
	public String list() {
		return "system/sysConfigList";
	}

	/**
	 * 获取用户json
	 */
	@RequestMapping(value="json",method = RequestMethod.GET)
	@ResponseBody
	public Map<String, Object> getData(HttpServletRequest request) {
		Page<SysConfig> page = getPage(request);
		List<PropertyFilter> filters = PropertyFilter.buildFromHttpRequest(request);
		page = sysConfigService.search(page, filters);
		return getEasyUIData(page);
	}

	/**
	 * 添加医生
	 * @param model
	 */
	@RequestMapping(value = "create", method = RequestMethod.GET)
	public String createForm(Model model) {
		model.addAttribute("sysConfig", new SysConfig());
		model.addAttribute("action", "create");
		return "system/sysConfigForm";
	}

	/**
	 * 添加医生 
	 * @param user
	 * @param model
	 */
	@RequestMapping(value = "create", method = RequestMethod.POST)
	@ResponseBody
	public String create(@Valid SysConfig sysConfig, Model model) {
		sysConfigService.save(sysConfig);
		return "success";
	}

	/**
	 * 修改医生跳转
	 * @param id
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "update/{id}", method = RequestMethod.GET)
	public String updateForm(@PathVariable("id") Long id, Model model) {
		model.addAttribute("sysConfig", sysConfigService.get(id));
		model.addAttribute("action", "update");
		return "system/sysConfigForm";
	}

	/**
	 * 修改医生
	 * 
	 * @param sysConfig
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "update", method = RequestMethod.POST)
	@ResponseBody
	public String update(@Valid @ModelAttribute @RequestBody SysConfig sysConfig,Model model) {
		sysConfigService.update(sysConfig);
		return "success";
	}

	/**
	 * 删除医生
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "delete/{id}")
	@ResponseBody
	public String delete(@PathVariable("id") Long id) {
		sysConfigService.delete(id);
		return "success";
	}
	
	/**
	 * 所有RequestMapping方法调用前的Model准备方法, 实现Struts2
	 * Preparable二次部分绑定的效果,先根据form的id从数据库查出Task对象,再把Form提交的内容绑定到该对象上。
	 * 因为仅update()方法的form中有id属性，因此仅在update时实际执行.
	 */
	@ModelAttribute
	public void getModel(@RequestParam(value = "id", defaultValue = "-1") Long id,Model model) {
		if (id != -1) {
			model.addAttribute("sysConfig", sysConfigService.get(id));
		}
	}

}
