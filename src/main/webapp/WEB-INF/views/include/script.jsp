<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
    <script src="${ctx}/static/plugins/hplusV4.1.0/js/jquery.min.js?v=2.1.4"></script>
    <script src="${ctx}/static/plugins/hplusV4.1.0/js/bootstrap.min.js?v=3.3.6"></script>
     <script src="${ctx}/static/plugins/hplusV4.1.0/js/content.min.js?v=1.0.0"></script>
    <script src="${ctx}/static/plugins/hplusV4.1.0/js/plugins/bootstrap-table/bootstrap-table.min.js"></script>
    <script src="${ctx}/static/plugins/hplusV4.1.0/js/plugins/bootstrap-table/bootstrap-table-mobile.min.js"></script>
    <script src="${ctx}/static/plugins/hplusV4.1.0/js/plugins/bootstrap-table/locale/bootstrap-table-zh-CN.min.js"></script>
    <script type="${ctx}/static/plugins/hplusV4.1.0/text/javascript" src="http://tajs.qq.com/stats?sId=9051096" charset="UTF-8"></script>
    <script src="${ctx}/static/plugins/hplusV4.1.0/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="${ctx}/static/plugins/hplusV4.1.0/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="${ctx}/static/plugins/hplusV4.1.0/js/plugins/layer/layer.min.js"></script>
    <script src="${ctx}/static/plugins/hplusV4.1.0/js/hplus.min.js?v=4.1.0"></script>
    <script type="text/javascript" src="${ctx}/static/plugins/hplusV4.1.0/js/contabs.min.js"></script>
    <script src="${ctx}/static/plugins/hplusV4.1.0/js/plugins/pace/pace.min.js"></script>
    <script src="${ctx}/static/plugins/hplusV4.1.0/js/plugins/toastr/toastr.min.js"></script>
    <script>
//全局的AJAX访问，处理AJAX清求时SESSION超时
$.ajaxSetup({
    contentType:"application/x-www-form-urlencoded;charset=utf-8",
    complete:function(XMLHttpRequest,textStatus){
          //通过XMLHttpRequest取得响应头，sessionstatus           
          var sessionstatus=XMLHttpRequest.getResponseHeader("sessionstatus"); 
          if(sessionstatus=="timeout"){
               //跳转的登录页面
               if(window.top !== window.self){
            	   window.location.replace('${ctx}/a/login');
               }else{
            	   window.top.location.replace('${ctx}/a/login');
               }
               
       		}	
    }
});
</script>