<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<c:set var="ctxAddr" value="${pageContext.request.remoteHost}"/>
<c:set var="ctxPort" value="${pageContext.request.localPort}"/>
    <link rel="shortcut icon" href="favicon.ico">
    <link href="${ctx}/static/plugins/hplusV4.1.0/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="${ctx}/static/plugins/hplusV4.1.0/css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
    <link href="${ctx}/static/plugins/hplusV4.1.0/css/plugins/bootstrap-table/bootstrap-table.min.css" rel="stylesheet">
    <link href="${ctx}/static/plugins/hplusV4.1.0/css/animate.min.css" rel="stylesheet">
    <link href="${ctx}/static/plugins/hplusV4.1.0/css/style.css?v=4.1.0" rel="stylesheet">
     <link href="${ctx}/static/plugins/hplusV4.1.0/css/plugins/toastr/toastr.min.css" rel="stylesheet">   
<!--[if lt IE 9]>
    <meta http-equiv="refresh" content="0;ie.html" />
    <![endif]-->