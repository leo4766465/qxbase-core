<!-- Stylesheets -->
<link href="static/plugins/mac-Bootstrap/style/bootstrap.css" rel="stylesheet">
<!-- Font awesome icon -->
<link rel="stylesheet" href="static/plugins/mac-Bootstrap/style/font-awesome.css">
<!-- jQuery UI -->
<link rel="stylesheet" href="static/plugins/mac-Bootstrap/style/jquery-ui.css">
<!-- Calendar -->
<link rel="stylesheet" href="static/plugins/mac-Bootstrap/style/fullcalendar.css">
<!-- prettyPhoto -->
<link rel="stylesheet" href="static/plugins/mac-Bootstrap/style/prettyPhoto.css">
<!-- Star rating -->
<link rel="stylesheet" href="static/plugins/mac-Bootstrap/style/rateit.css">
<!-- Date picker -->
<link rel="stylesheet" href="static/plugins/mac-Bootstrap/style/bootstrap-datetimepicker.min.css">
<!-- CLEditor -->
<link rel="stylesheet" href="static/plugins/mac-Bootstrap/style/jquery.cleditor.css">
<!-- Uniform -->
<link rel="stylesheet" href="static/plugins/mac-Bootstrap/style/uniform.default.css">
<!-- Bootstrap toggle -->
<link rel="stylesheet" href="static/plugins/mac-Bootstrap/style/bootstrap-switch.css">
<!-- Main stylesheet -->
<link href="static/plugins/mac-Bootstrap/style/style.css" rel="stylesheet">
<!-- Widgets stylesheet -->
<link href="static/plugins/mac-Bootstrap/style/widgets.css" rel="stylesheet">
<!-- HTML5 Support for IE -->
<!--[if lt IE 9]>
  <script src="static/plugins/mac-Bootstrap/js/html5shim.js"></script>
  <![endif]-->
<!-- Favicon -->
<link rel="shortcut icon" href="static/plugins/mac-Bootstrap/img/favicon/favicon.png">
<!-- Scroll to top -->
<span class="totop"><a href="#"><i class="icon-chevron-up"></i></a></span> 
<!-- JS -->
<script src="static/plugins/mac-Bootstrap/js/jquery.js"></script>
<!-- jQuery -->
<script src="static/plugins/mac-Bootstrap/js/bootstrap.js"></script>
<!-- Bootstrap -->
<script src="static/plugins/mac-Bootstrap/js/jquery-ui-1.9.2.custom.min.js"></script>
<!-- jQuery UI -->
<script src="static/plugins/mac-Bootstrap/js/fullcalendar.min.js"></script>
<!-- Full Google Calendar - Calendar -->
<script src="static/plugins/mac-Bootstrap/js/jquery.rateit.min.js"></script>
<!-- RateIt - Star rating -->
<script src="static/plugins/mac-Bootstrap/js/jquery.prettyPhoto.js"></script>
<!-- prettyPhoto -->

<!-- jQuery Flot -->
<script src="static/plugins/mac-Bootstrap/js/excanvas.min.js"></script>
<script src="static/plugins/mac-Bootstrap/js/jquery.flot.js"></script>
<script src="static/plugins/mac-Bootstrap/js/jquery.flot.resize.js"></script>
<script src="static/plugins/mac-Bootstrap/js/jquery.flot.pie.js"></script>
<script src="static/plugins/mac-Bootstrap/js/jquery.flot.stack.js"></script>

<!-- jQuery Notification - Noty -->
<script src="static/plugins/mac-Bootstrap/js/jquery.noty.js"></script>
<!-- jQuery Notify -->
<script src="static/plugins/mac-Bootstrap/js/themes/default.js"></script>
<!-- jQuery Notify -->
<script src="static/plugins/mac-Bootstrap/js/layouts/bottom.js"></script>
<!-- jQuery Notify -->
<script src="static/plugins/mac-Bootstrap/js/layouts/topRight.js"></script>
<!-- jQuery Notify -->
<script src="static/plugins/mac-Bootstrap/js/layouts/top.js"></script>
<!-- jQuery Notify -->
<!-- jQuery Notification ends -->
<script src="static/plugins/mac-Bootstrap/js/sparklines.js"></script>
<!-- Sparklines -->
<script src="static/plugins/mac-Bootstrap/js/jquery.cleditor.min.js"></script>
<!-- CLEditor -->
<script src="static/plugins/mac-Bootstrap/js/bootstrap-datetimepicker.min.js"></script>
<!-- Date picker -->
<script src="static/plugins/mac-Bootstrap/js/jquery.uniform.min.js"></script>
<!-- jQuery Uniform -->
<script src="static/plugins/mac-Bootstrap/js/bootstrap-switch.min.js"></script>
<!-- Bootstrap Toggle -->
<script src="static/plugins/mac-Bootstrap/js/filter.js"></script>
<!-- Filter for support page -->
<script src="static/plugins/mac-Bootstrap/js/custom.js"></script>
<!-- Custom codes -->
<script src="static/plugins/mac-Bootstrap/js/charts.js"></script>
<!-- Charts & Graphs -->