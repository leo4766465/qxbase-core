<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title>上传图片</title>
<%@ include file="/WEB-INF/views/include/upload.jsp"%>
</head>
<body>
<div style="padding:15px;">
	<form id="mainform" action="${ctx}/system/upload/up" method="post">
		<input id="file_upload" type="file" name="file"/></br></br>
        <a href="javascript:$('#file_upload').uploadify('upload', '*')">上传文件</a> | <a href="javascript:$('#file_upload').uploadify('stop')">停止上传!</a>
	</form>
</div>

<script type="text/javascript">
//提交表单
$(document).ready(function(){
    $(function() {
        $('#file_upload').uploadify({
            'swf'      : '${ctx}/static/plugins/uploadify/uploadify.swf',
            'uploader' : '${ctx}/system/upload/up',
            'height': 25,
            'whith' :120,
            'auto'  : true,
            'fileDataName':'file',
            'buttonText' : '选择图片...',
            'fileTypeExts' : '*.gif; *.jpg; *.png',
            'multi'    : true,
            'method'   :'post',
            'debug':true,
           // 'onUploadStart' : function(file) {
              	//var param = {};
               // param.picHref = $('#file_upload_href').val();
               // $("#file_upload").uploadify("settings", "formData", param);
           // },
            'onUploadSuccess' : function(file, data, response) {
            	alert(data);
            	/* var imgUrl = uploadCommon.getPath(data);
                $("#imgUrl").val(imgUrl);// 返回的图片路径保存起来
                $("#thumbImg").attr("src", IMAGE_FILE_PATH + imgUrl);// 更新logo显示
                uploadCommon.uploadImageBtnStyle("imgUrl");
                uploadCommon.initPreviewAfterUpload(data); // 新图片预览*/            
            },
            'onUploadError' : function(file, errorCode, errorMsg, errorString) {
                alert('The file ' + file.name + ' could not be uploaded: ' + errorString);
            } 
        });
    });
}); 
</script>
</body>
</html>