<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title></title>
<%@ include file="/WEB-INF/views/include/easyui.jsp"%>
<link href="${ctx}/static/plugins/hplusV4.1.0/css/plugins/sweetalert/sweetalert.css" rel="stylesheet"/>
<script src="${ctx}/static/plugins/hplusV4.1.0/js/plugins/sweetalert/sweetalert.min.js"></script>
</head>
<body style="font-family: '微软雅黑'">
<div id="tb" style="padding:5px;height:auto">
    <div>
    	<shiro:hasPermission name="sys:perm:add">
    	<a href="#" class="easyui-linkbutton" plain="true" iconCls="icon-add" onclick="add();">添加</a>
    	<span class="toolbar-item dialog-tool-separator"></span>
    	</shiro:hasPermission>
        <shiro:hasPermission name="sys:perm:delete">
        <a href="#" class="easyui-linkbutton" plain="true" iconCls="icon-remove" onclick="del()">删除</a>
        <span class="toolbar-item dialog-tool-separator"></span>
        </shiro:hasPermission>
        <shiro:hasPermission name="sys:perm:update">
        <a href="#" class="easyui-linkbutton" plain="true" iconCls="icon-edit" onclick="upd()">修改</a>
        </shiro:hasPermission>
    </div>
</div>
<table id="dg"></table>
<div id="dlg"></div> 

<script type="text/javascript">
var dg;
var d;
var parentPermId;
$(function(){   
	dg=$('#dg').treegrid({  
	method: "get",
    url:'${ctx}/system/permission/menu/json', 
    fit : true,
	fitColumns : true,
	border : false,
	idField : 'id',
	treeField:'name',
	parentField : 'pid',
	iconCls: 'icon',
	animate:true, 
	rownumbers:true,
	singleSelect:true,
	striped:true,
    columns:[[    
        {field:'id',title:'id',hidden:true,width:100},    
        {field:'name',title:'名称',width:100},
        {field:'url',title:'资源路径',width:100},
        {field:'node',title:'节点属性',
			formatter : function(value, row, index) {
				return value == 'f' ? '功能节点' : value=='d'?'目录节点':'操作';
			}
        },
        {field:'sort',title:'排序'},
        {field:'description',title:'描述',width:100}
    ]],
    toolbar:'#tb',
    dataPlain: true
	});
	
});

//弹窗增加
function add() {
	//父级权限
	var row = dg.treegrid('getSelected');
	if(row){
		parentPermId=row.id;
	}
	d=$('#dlg').dialog({    
	    title: '添加菜单',    
	    width: 450,    
	    height: 320,    
	    closed: false,    
	    cache: false,
	    maximizable:true,
	    resizable:true,
	    href:'${ctx}/system/permission/menu/create',
	    modal: true,
	    buttons:[{
			text:'确认',
			handler:function(){
				$("#mainform").submit();
			}
		},{
			text:'取消',
			handler:function(){
				d.panel('close');
			}
		}]
	});
}

//删除
function del(){
	var row = dg.treegrid('getSelected');
	if(row==null){
		alert('请选择一个节点!');
		return;
	} 
	swal({
		title : "您确定要删除这条信息吗",
		text : "删除后将无法恢复，请谨慎操作！",
		type : "warning",
		showCancelButton : true,
		confirmButtonColor : "#DD6B55",
		confirmButtonText : "删除",
		closeOnConfirm : true
	}, function() {
		$.ajax({
			type:'get',
			url:"${ctx}/system/permission/delete/"+row.id,
			success: function(data){
				swal("删除成功！", "您已经永久删除了这条信息。", "success");
				dg.treegrid("unselectAll");
		    	dg.treegrid('reload');
			}
		});
	});		
}

//修改
function upd(){
	var row = dg.treegrid('getSelected');
	if(row==null){
		alert('请选择一个节点!');
		return;
	} 	
	//父级权限
	parentPermId=row.pid;
	d=$("#dlg").dialog({   
	    title: '修改菜单',    
	    width: 450,    
	    height: 320,    
	    href:'${ctx}/system/permission/menu/update/'+row.id,
	    maximizable:true,
	    modal:true,
	    buttons:[{
			text:'确认',
			handler:function(){
				$("#mainform").submit();
			}
		},{
			text:'取消',
			handler:function(){
				d.panel('close');
			}
		}]
	});
}
</script>
</body>
</html>