<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title></title>
<%@ include file="/WEB-INF/views/include/css.jsp"%>
<link href="${ctx}/static/plugins/hplusV4.1.0/css/plugins/datapicker/datepicker3.css" rel="stylesheet"/>
<link href="${ctx}/static/plugins/hplusV4.1.0/css/plugins/sweetalert/sweetalert.css" rel="stylesheet"/>
</head>
<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="ibox float-e-margins">
			<div class="panel-body" style="padding-bottom: 0px;">
				<div class="col-sm-12">
					<div class="example-wrap">
						<div class="example">
							<div class="panel panel-default">
								<div class="panel-heading">查询条件</div>
								<div class="panel-body">
									<form id="formSearch" class="form-horizontal" role="form">

										<div class="form-group" id="date_range">
											<label class="control-label col-sm-1"
												for="filter_LIKES_phone">手机号:</label>
											<div class="input-group col-sm-2" style="float: left;">
												<input type="text" class="form-control"
													id="filter_LIKES_phone" name="filter_LIKES_phone" />
											</div>
											<label class="control-label col-sm-2">日期范围：</label>
											<div class="input-daterange input-group col-sm-3">
												<input type="text" class="input-sm form-control"
													id="filter_GTD_createDate" name="start" /> <span
													class="input-group-addon">到</span> <input type="text"
													class="input-sm form-control" id="filter_LTD_createDate"
													name="end" />
											</div>

										</div>
										<div class="form-group" style="margin-top: 15px">
											<label class="control-label col-sm-1"
												for="filter_LIKES_loginName">账号:</label>
											<div class="input-group col-sm-2" style="float: left;">
												<input type="text" class="form-control"
													id="filter_LIKES_loginName" name="filter_LIKES_loginName" />
											</div>
											<label class="control-label col-sm-1" for="filter_LIKES_name">昵称:</label>
											<div class="input-group col-sm-2" style="float: left;">
												<input type="text" class="form-control"
													id="filter_LIKES_name" name="filter_LIKES_name" />
											</div>
											<div class="input-group col-sm-3" style="text-align: left;">
												<button type="button" style="margin-left: 50px" id="search"
													class="btn btn-primary">查询</button>
											</div>
										</div>
									</form>
								</div>
							</div>

							<div id="toolbar" class="btn-group hidden-xs" role="group">
								<shiro:hasPermission name="sys:user:add">
									<button id="btn_add" type="button"
										class="btn btn-outline btn-default">
										<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>新增
									</button>
								</shiro:hasPermission>
								<shiro:hasPermission name="sys:user:update">
									<button id="btn_edit" type="button"
										class="btn btn-outline btn-default">
										<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>修改
									</button>
								</shiro:hasPermission>
								<shiro:hasPermission name="sys:user:delete">
									<button id="btn_delete" type="button"
										class="btn btn-outline btn-default">
										<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>删除
									</button>
								</shiro:hasPermission>
								<shiro:hasPermission name="sys:user:roleView">
									<button id="btn_role" type="button"
										class="btn btn-outline btn-default">
										<span class="glyphicon glyphicon-user" aria-hidden="true"></span>用户角色
									</button>
								</shiro:hasPermission>
							</div>
							<div class="ibox-content">
								<div class="row row-lg">
									<div class="col-sm-12">
										<table id="dg" data-mobile-responsive="true"></table>
										<div id="dlg"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<%@ include file="/WEB-INF/views/include/script.jsp"%>
 <script src="${ctx}/static/plugins/hplusV4.1.0/js/plugins/datapicker/bootstrap-datepicker.js"></script>
<script src="${ctx}/static/plugins/hplusV4.1.0/js/plugins/sweetalert/sweetalert.min.js"></script>
	<script type="text/javascript">
		toastr.options = {
			"closeButton" : true,
			"debug" : true,
			"progressBar" : false,
			"positionClass" : "toast-bottom-right",
			"onclick" : null,
			"showDuration" : "400",
			"hideDuration" : "1000",
			"timeOut" : "700",
			"extendedTimeOut" : "1000",
			"showEasing" : "swing",
			"hideEasing" : "linear",
			"showMethod" : "fadeIn",
			"hideMethod" : "fadeOut"
		}
		var $table = $('#dg');
		$(function() {
			//1.初始化Table
			var oTable = new TableInit();
			oTable.Init();
			//2.初始化Button的点击事件
			var oButtonInit = new ButtonInit();
			oButtonInit.Init();
			$("#search").bind("click", function() {
				$('#dg').bootstrapTable('destroy');
				oTable.Init();
			});
		});

		var TableInit = function() {
			var oTableInit = new Object();
			//初始化Table
			oTableInit.Init = function() {
				$table.bootstrapTable({
					url : '${ctx}/system/user/json', //请求后台的URL（*）
					method : 'get', //请求方式（*）
					iconSize : 'outline',
					toolbar : '#toolbar', //工具按钮用哪个容器
					striped : true, //是否显示行间隔色
					cache : false, //是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
					pagination : true, //是否显示分页（*）
					sortable : false, //是否启用排序
					sortOrder : "asc", //排序方式
					queryParams : oTableInit.queryParams,//传递参数（*）
					sidePagination : "server", //分页方式：client客户端分页，server服务端分页（*）
					pageNumber : 1, //初始化加载第一页，默认第一页
					pageSize : 10, //每页的记录行数（*）
					pageList : [ 10, 25, 50, 100 ], //可供选择的每页的行数（*）
					search : false, //是否显示表格搜索，此搜索是客户端搜索，不会进服务端，所以，个人感觉意义不大
					strictSearch : true,
					sortable : true,
					showColumns : true, //是否显示所有的列
					showRefresh : true, //是否显示刷新按钮
					minimumCountColumns : 2, //最少允许的列数
					clickToSelect : true, //是否启用点击选中行
					uniqueId : "id", //每一行的唯一标识，一般为主键列
					showToggle : true, //是否显示详细视图和列表视图的切换按钮
					clickToSelect : true,
					cardView : false, //是否显示详细视图
					detailView : false, //是否显示父子表
					showPaginationSwitch : true,
					//showExport: true,                     //是否显示导出
					// exportDataType: "basic",              //basic', 'all', 'selected'.
					columns : [ {
						field : 'state',
						radio : true,
						align : 'center',
						valign : 'middle'
					}, {
						field : 'id',
						title : 'id',
						align : 'center',
						valign : 'middle',
						visible : false
					}, {
						field : 'loginName',
						title : '帐号',
						align : 'center',
						valign : 'middle',
						sortable : true,
						width : 100
					}, {
						field : 'name',
						title : '昵称',
						align : 'center',
						valign : 'middle',
						sortable : true,
						width : 100
					}, {
						field : 'gender',
						title : '性别',
						align : 'center',
						valign : 'middle',
						sortable : true,
						formatter : function(value, row, index) {
							if(value==1){
								return '男';	
							}else if(value==0){
								return '女';
							}else{
								return '';
							}
							
						}
					}, {
						field : 'email',
						title : 'email',
						align : 'center',
						valign : 'middle',
						sortable : true,
						width : 100
					}, {
						field : 'phone',
						title : '电话',
						align : 'center',
						valign : 'middle',
						sortable : true,
						width : 100
					}, {
						field : 'loginCount',
						title : '登录次数',
						align : 'center',
						valign : 'middle',
						sortable : true
					}, {
						field : 'previousVisit',
						title : '上一次登录',
						align : 'center',
						valign : 'middle',
						sortable : true
					} ],
					formatLoadingMessage : function() {
						return "请稍等，正在加载中...";
					},
					formatNoMatches : function() { //没有匹配的结果  
						return '无符合条件的记录';
					},
					onLoadError : function(data) {
						$('#dg').bootstrapTable('removeAll');
					},
				});
			};
			//得到查询的参数
			oTableInit.queryParams = function(params) {
				var temp = { //这里的键的名字和控制器的变量名必须一直，这边改动，控制器也需要改成一样的  //页面大小
					filter_GTD_createDate : $("#filter_GTD_createDate").val(),
					filter_LTD_createDate : $("#filter_LTD_createDate").val(),
					filter_LIKES_loginName : $("#filter_LIKES_loginName").val(),
					filter_LIKES_name : $("#filter_LIKES_name").val(),
					filter_LIKES_phone : $("#filter_LIKES_phone").val(),
					page : params.offset/params.limit+1, //页码
					rows : params.limit,
					sort : params.sort,
					order : params.order
				};
				return temp;
			};
			return oTableInit;
		};

		var ButtonInit = function() {
			var oInit = new Object();
			var postdata = {};
			oInit.Init = function() {
				$("#btn_add").click(add);
				$("#btn_edit").click(upd);
				$("#btn_delete").click(del);
				$("#btn_role").click(userForRole);
				// $("#btn_org").click(userForOrg);
			};
			return oInit;
		};

		$("#date_range .input-daterange").datepicker({
			keyboardNavigation : !1,
			forceParse : !1,
			autoclose : !0
		});

		function getIdSelections() {
			return $.map($table.bootstrapTable('getSelections'), function(row) {
				return row.id
			});
		}

		//弹窗增加
		function add() {
			layer.open({
				type : 2,
				title : '添加用户',
				area : [ '500px', '400px' ],
				content : '${ctx}/system/user/create',
				btn : [ '确认', '取消' ],
				yes : function(index, layero) {
					var iframeWin = window[layero.find('iframe')[0]['name']];
					iframeWin.bt_submit();
				}
			});
		}

		//删除
		function del() {
			var row = getIdSelections();
			if (row.length <= 0) {
				toastr.warning('请选择有效数据');
				return;
			}
			swal({
				title : "您确定要删除这条信息吗",
				text : "删除后将无法恢复，请谨慎操作！",
				type : "warning",
				showCancelButton : true,
				confirmButtonColor : "#DD6B55",
				confirmButtonText : "删除",
				closeOnConfirm : false
			}, function() {
				$.ajax({
					type : "get",
					url : "${ctx}/system/user/delete/" + row[0],
					success : function(data, status) {
						if (status == "success") {
							swal("删除成功！", "您已经永久删除了这条信息。", "success");
							$table.bootstrapTable('refresh');
						}
					},
					error : function() {
						toastr.error('删除遇到错误了');
					},
				});

			});
		}

		//弹窗修改
		function upd() {
			var row = getIdSelections();
			console.log(JSON.stringify(row));
			if (row.length <= 0) {
				toastr.warning('请选择有效数据');
				return;
			}
			layer.open({
				type : 2,
				title : '修改用户',
				area : [ '500px', '400px' ],
				content : '${ctx}/system/user/update/'+row[0],
				btn : [ '确认', '取消' ],
				yes : function(index, layero) {
					var iframeWin = window[layero.find('iframe')[0]['name']];
					iframeWin.bt_submit();
				}
			});			
		}

		//用户角色弹窗
		function userForRole() {
			var row = getIdSelections();
			console.log(JSON.stringify(row));
			if (row.length <= 0) {
				toastr.warning('请选择有效数据');
				return;
			}
			layer.open({
				type : 2,
				title : '用户角色',
				area : [ '500px', '400px' ],
				content : '${ctx}/system/user/'+row[0]+'/userRole',
				btn : [ '确认', '取消' ],
				yes : function(index, layero) {
					var iframeWin = window[layero.find('iframe')[0]['name']];
					iframeWin.saveUserRole();
				}
			});
		}
		//用户机构弹窗
		function userForOrg() {
			var row = getIdSelections();
			if (row.length <= 0) {
				toastr.warning('请选择有效数据');
				return;
			}
			layer.open({
				type : 2,
				title : '用户角色',
				area : [ '500px', '400px' ],
				content : '${ctx}/system/user/'+row[0]+'/userOrg',
				btn : [ '确认', '取消' ],
				yes : function(index, layero) {
					var iframeWin = window[layero.find('iframe')[0]['name']];
					iframeWin.saveUserOrg();
				}
			});		
		}
		
		//创建查询对象并查询
		function cx() {
			var obj = $("#searchFrom").serializeObject();
			dg.datagrid('load', obj);
		}
	</script>
</body>
</html>