<%@ page contentType="text/html;charset=UTF-8" %>
<%@ page import="org.apache.shiro.web.filter.authc.FormAuthenticationFilter"%>
<%@ page import="org.apache.shiro.authc.ExcessiveAttemptsException"%>
<%@ page import="org.apache.shiro.authc.IncorrectCredentialsException"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<%
String error = (String) request.getAttribute(FormAuthenticationFilter.DEFAULT_ERROR_KEY_ATTRIBUTE_NAME);
request.setAttribute("error", error);
%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>H+ 后台主题UI框架 - 登录</title>
    <meta name="keywords" content="H+后台主题,后台bootstrap框架,会员中心主题,后台HTML,响应式后台">
    <meta name="description" content="H+是一个完全响应式，基于Bootstrap3最新版本开发的扁平化主题，她采用了主流的左右两栏式布局，使用了Html5+CSS3等现代技术">

    <link rel="shortcut icon" href="${ctx}/static/plugins/hplusV4.1.0/favicon.ico"> 
    <link href="${ctx}/static/plugins/hplusV4.1.0/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="${ctx}/static/plugins/hplusV4.1.0/css/font-awesome.css?v=4.4.0" rel="stylesheet">
	<script src="${ctx}/static/plugins/easyui/jquery/jquery-1.11.1.min.js"></script>
    <link href="${ctx}/static/plugins/hplusV4.1.0/css/animate.css" rel="stylesheet">
    <link href="${ctx}/static/plugins/hplusV4.1.0/css/style.css?v=4.1.0" rel="stylesheet">
    <!--[if lt IE 9]>
    <meta http-equiv="refresh" content="0;ie.html" />
    <![endif]-->
    <script>if(window.top !== window.self){ window.top.location = window.location;}
    console.log('刷新');
    refreshCaptcha();
    console.log('++++++++++++++++++++++++++==刷新');
	function refreshCaptcha(){  
	    // document.getElementById("img_captcha").src="${ctx}/static/images/kaptcha.jpg?t=" + Math.random();  
		$("#img_captcha").attr("src","${ctx}/static/images/kaptcha.jpg?t="+ Math.random());    
	}  
	</script>
</head>

<body class="gray-bg">

    <div class="middle-box text-center loginscreen  animated fadeInDown">
        <div>
            <div>
                <h1 class="logo-name">H+</h1>
            </div>
            <h3>欢迎使用 H+</h3>

            <form class="m-t" role="form" action="${ctx}/a/login" method="post">
                <div class="form-group">
                    <input type="text" id="username" name="username" class="form-control" placeholder="用户名" required="">
                </div>
                <div class="form-group">
                    <input  id="password" name="password" type="password"  class="form-control" placeholder="密码" required="">
                </div>
                <div class="form-group">
				<input type="text" id="captcha" name="captcha"class="form-control" style="width:40%;" placeholder="验证码" />
				<div style="width:60%; float:right;margin-top:-35px; display:inline-block;"><img alt="验证码" src="${ctx}/static/images/kaptcha.jpg" title="点击更换" id="img_captcha" onclick="javascript:refreshCaptcha();" style="height:35px;width:85px;" />
				</div>
				</div><div class="login_main_errortip form-group">&nbsp;</div>                
                <button type="submit" class="btn btn-primary block full-width m-b">登 录</button>
                <p class="text-muted text-center"> <a href="login.html#"><small>忘记密码了？</small></a> | <a href="register.html">注册一个新账号</a>
                </p>
            </form>
        </div>
    </div>
    <!-- 全局js -->
    <script src="${ctx}/static/plugins/hplusV4.1.0/js/jquery.min.js?v=2.1.4"></script>
    <script src="${ctx}/static/plugins/hplusV4.1.0/js/bootstrap.min.js?v=3.3.6"></script>
    <script type="${ctx}/static/plugins/hplusV4.1.0/text/javascript" src="http://tajs.qq.com/stats?sId=9051096" charset="UTF-8"></script>
    <!--统计代码，可删除-->
   <c:choose>
		<c:when test="${error eq 'com.qingxun.core.util.CaptchaException'}">
			<script>
				$(".login_main_errortip").html("验证码错误，请重试");
			</script>
		</c:when>
		<c:when test="${error eq 'org.apache.shiro.authc.UnknownAccountException'}">
			<script>
				$(".login_main_errortip").html("用户名不存在，请重试");
			</script>
		</c:when>
		<c:when test="${error eq 'org.apache.shiro.authc.IncorrectCredentialsException'}">
			<script>
				$(".login_main_errortip").html("帐号或密码错误，请重试");
			</script>
		</c:when>
	</c:choose>
</body>
</html>
