<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title></title>
<%@ include file="/WEB-INF/views/include/easyui.jsp"%>
</head>
<body>
<font color='blue'>蜂巢接口列表</font>
<table id="dg">
<c:forEach items="${apiList}" var="item">
	<tr>
		<td>接口名称:<font color="red">${item.apiName}</font></td>
	</tr>
	<tr>
		<td>接口Url:${item.url}</td>
	</tr>	
	<tr>
		<td>传入参数:${item.inParam}</td>
	</tr>
	<tr>
		<td style="border-bottom:1px dashed #000">返回参数:${item.outParam}</td>
	</tr>		
</c:forEach>
</table> 

</body>
</html>