<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title></title>
<%@ include file="/WEB-INF/views/include/css.jsp"%>
<link href="${ctx}/static/plugins/hplusV4.1.0/css/plugins/datapicker/datepicker3.css" rel="stylesheet"/>
<link href="${ctx}/static/plugins/hplusV4.1.0/css/plugins/sweetalert/sweetalert.css" rel="stylesheet"/>
</head>
<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="ibox float-e-margins">
			<div class="panel-body" style="padding-bottom: 0px;">
				<div class="col-sm-12">
					<div class="example-wrap">
						<div class="example">
							<div class="panel panel-default">
								<div class="panel-heading">查询条件</div>
								<div class="panel-body">
									<form id="formSearch" class="form-horizontal" role="form">
										<div class="form-group" id="date_range">
											<label class="control-label col-sm-2"
												for="k">关键字搜索:</label>
											<div class="input-group col-sm-2" style="float: left;">
												<input type="text" class="form-control"
													id="k" name="k" />
											</div>	
											<div class="input-group col-sm-2" style="text-align: left;">
												<button type="button" style="margin-left: 50px" id="search"
													class="btn btn-primary">查询</button>
											</div>																					
										</div>	
									</form>
								</div>
							</div>

							<div id="toolbar" class="btn-group hidden-xs" role="group">
								<shiro:hasPermission name="sys:log:delete">
									<button id="btn_delete" type="button"
										class="btn btn-outline btn-default">
										<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>删除
									</button>
								</shiro:hasPermission>
								<shiro:hasPermission name="sys:log:exportExcel">
									<button id="btn_export" type="button"
										class="btn btn-outline btn-default">
										<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>导出
									</button>
								</shiro:hasPermission>

							</div>
							<div class="ibox-content">
								<div class="row row-lg">
									<div class="col-sm-12">
										<table id="dg" data-mobile-responsive="true"></table>
										<div id="dlg"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<%@ include file="/WEB-INF/views/include/script.jsp"%>
 <script src="${ctx}/static/plugins/hplusV4.1.0/js/plugins/datapicker/bootstrap-datepicker.js"></script>
<script src="${ctx}/static/plugins/hplusV4.1.0/js/plugins/sweetalert/sweetalert.min.js"></script>
	<script type="text/javascript">
		toastr.options = {
			"closeButton" : true,
			"debug" : true,
			"progressBar" : false,
			"positionClass" : "toast-bottom-right",
			"onclick" : null,
			"showDuration" : "400",
			"hideDuration" : "1000",
			"timeOut" : "700",
			"extendedTimeOut" : "1000",
			"showEasing" : "swing",
			"hideEasing" : "linear",
			"showMethod" : "fadeIn",
			"hideMethod" : "fadeOut"
		}
		var $table = $('#dg');
		$(function() {
			//1.初始化Table
			var oTable = new TableInit();
			oTable.Init();
			//2.初始化Button的点击事件
			var oButtonInit = new ButtonInit();
			oButtonInit.Init();
			$("#search").bind("click", function() {
				$('#dg').bootstrapTable('destroy');
				oTable.Init();
			});
		});

		var TableInit = function() {
			var oTableInit = new Object();
			//初始化Table
			oTableInit.Init = function() {
				$table.bootstrapTable({
					url : '${ctx}/system/log/jsonForSearch', //请求后台的URL（*）
					method : 'get', //请求方式（*）
					iconSize : 'outline',
					toolbar : '#toolbar', //工具按钮用哪个容器
					striped : true, //是否显示行间隔色
					cache : false, //是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
					pagination : true, //是否显示分页（*）
					sortable : false, //是否启用排序
					sortOrder : "asc", //排序方式
					queryParams : oTableInit.queryParams,//传递参数（*）
					sidePagination : "server", //分页方式：client客户端分页，server服务端分页（*）
					pageNumber : 1, //初始化加载第一页，默认第一页
					pageSize : 10, //每页的记录行数（*）
					pageList : [ 10, 25, 50, 100 ], //可供选择的每页的行数（*）
					search : false, //是否显示表格搜索，此搜索是客户端搜索，不会进服务端，所以，个人感觉意义不大
					strictSearch : true,
					sortable : true,
					showColumns : true, //是否显示所有的列
					showRefresh : true, //是否显示刷新按钮
					minimumCountColumns : 2, //最少允许的列数
					clickToSelect : true, //是否启用点击选中行
					height : 500, //行高，如果没有设置height属性，表格自动根据记录条数觉得表格高度
					uniqueId : "id", //每一行的唯一标识，一般为主键列
					showToggle : true, //是否显示详细视图和列表视图的切换按钮
					clickToSelect : true,
					cardView : false, //是否显示详细视图
					detailView : false, //是否显示父子表
					showPaginationSwitch : true,
					//showExport: true,                     //是否显示导出
					// exportDataType: "basic",              //basic', 'all', 'selected'.
					columns : [ {
						field : 'state',
						radio : true,
						align : 'center',
						valign : 'middle'
					}, {
						field : 'id',
						title : 'id',
						align : 'center',
						valign : 'middle',
						visible : false
					},{
						field : 'operationCode',
						title : '操作代码',
						align : 'center',
						valign : 'middle',
						sortable : true
					}, {
						field : 'requestParam',
						title : '参数',
						align : 'center',
						valign : 'middle',
						sortable : true
					}, {
						field : 'executeTime',
						title : '执行时间(mm)',
						align : 'center',
						valign : 'middle',
						sortable : true
					}, {
						field : 'ip',
						title : 'IP',
						align : 'center',
						valign : 'middle',
						sortable : true
					},{
						field : 'os',
						title : '操作系统',
						align : 'center',
						valign : 'middle',
						sortable : true
					},{
						field : 'browser',
						title : '浏览器',
						align : 'center',
						valign : 'middle',
						sortable : true
					},{
						field : 'createDate',
						title : '操作时间',
						align : 'center',
						valign : 'middle',
						sortable : true
					} ],
					formatLoadingMessage : function() {
						return "请稍等，正在加载中...";
					},
					formatNoMatches : function() { //没有匹配的结果  
						return '无符合条件的记录';
					},
					onLoadError : function(data) {
						$('#dg').bootstrapTable('removeAll');
					},
				});
			};
			//得到查询的参数
			oTableInit.queryParams = function(params) {
				var temp = { //这里的键的名字和控制器的变量名必须一直，这边改动，控制器也需要改成一样的  //页面大小
					pageNo : params.offset, //页码
					k : $("#k").val(),
					pageSize : params.limit,
					orderBy : params.sort,
					order : params.order
				};
				return temp;
			};
			return oTableInit;
		};

		var ButtonInit = function() {
			var oInit = new Object();
			var postdata = {};
			oInit.Init = function() {
				$("#btn_delete").click(del);
				$("#btn_export").click(exportExcel);
			};
			return oInit;
		};

		$("#date_range .input-daterange").datepicker({
			keyboardNavigation : !1,
			forceParse : !1,
			autoclose : !0
		});

		function getIdSelections() {
			return $.map($table.bootstrapTable('getSelections'), function(row) {
				return row.id
			});
		}

		//删除
		function del() {
			var row = getIdSelections();
			if (row.length <= 0) {
				toastr.warning('请选择有效数据');
				return;
			}
			swal({
				title : "您确定要删除这条信息吗",
				text : "删除后将无法恢复，请谨慎操作！",
				type : "warning",
				showCancelButton : true,
				confirmButtonColor : "#DD6B55",
				confirmButtonText : "删除",
				closeOnConfirm : false
			}, function() {
				$.ajax({
					type : "get",
					url : "${ctx}/system/log/delete/" + row[0],
					success : function(data, status) {
						if (status == "success") {
							swal("删除成功！", "您已经永久删除了这条信息。", "success");
							$table.bootstrapTable('refresh');
						}
					},
					error : function() {
						toastr.error('删除遇到错误了');
					},
				});

			});
		}
		
		//导出excel
		function exportExcel(){
			var url = "${ctx}/system/log/exportExcel";
			window.location.href = url;
		}		
	</script>
</body>
</html>